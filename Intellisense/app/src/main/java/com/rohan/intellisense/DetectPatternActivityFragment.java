package com.rohan.intellisense;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import pl.tajchert.sample.DotsTextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetectPatternActivityFragment extends Fragment {

    private static final String USERNAME = "Heidi";
    private View mDetectionSurface;
    private TextView mOutputTextView;
    private ArrayList<Point> points = new ArrayList<>();
    private TextToSpeech tts;
    private boolean mStarted;
    private TextView mTitleText;
    private DotsTextView mLoadingDots;

    public DetectPatternActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detect_pattern, container, false);
    }

    @Override
    public void onViewCreated(View view,
                              Bundle savedInstanceState) {

        mLoadingDots = (DotsTextView) view.findViewById(R.id.loading_dots);
        mTitleText = (TextView) view.findViewById(R.id.title_text);
        mDetectionSurface = view.findViewById(R.id.detection_surface);
        mOutputTextView = (TextView) view.findViewById(R.id.output_textview);
        mDetectionSurface.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int location[] = { 0, 0 };
                view.getLocationOnScreen(location);
                final int action = event.getAction();
                switch (action & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_POINTER_DOWN: {
                        points = new ArrayList<>();
                        for (int p = 0; p < event.getPointerCount(); p++) {
                            points.add(new Point(event.getX(p) + location[0], event.getY(p) + location[1]));
                        }

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!mStarted) {
                                    mStarted = true;
                                    detectPattern();
                                    mStarted = false;
                                }
                            }
                        }, 500);
                        break;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        break;
                    }
                    case MotionEvent.ACTION_POINTER_UP: {
                    }
                }
                return true;
            }
        });
        AlphaAnimation animation = new AlphaAnimation(0.8f, 0.5f);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setRepeatMode(AlphaAnimation.REVERSE);
        animation.setRepeatCount(AlphaAnimation.INFINITE);
        animation.setDuration(1000);
        mTitleText.setAnimation(animation);
        tts = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.SIMPLIFIED_CHINESE);
                    tts.speak("Good evening, place your prescription bottle on the phone.", TextToSpeech.QUEUE_FLUSH, null,null);
                }
            }
        });

        setupDisplayImage(view);
    }

    private void setupDisplayImage(View view) {
        Window window = getActivity().getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        int timeOfDay = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        timeOfDay = 15;
        ImageView imageView = (ImageView) view.findViewById(R.id.time_of_day_image);
        int drawableId = R.drawable.night;
        String greeting = "Good Night, ";
        String statusBarColor = "#161522";
        if(timeOfDay >= 5 && timeOfDay < 12){
            drawableId = R.drawable.morning;
            statusBarColor = "#92D4CB";
            greeting = "Good Morning, ";
        }else if(timeOfDay >= 12 && timeOfDay < 16){
            drawableId = R.drawable.afternoon;
            statusBarColor = "#97C59B";
            greeting = "Good Afternoon, ";
        }else if(timeOfDay >= 16 && timeOfDay < 20){
            drawableId = R.drawable.evening;
            statusBarColor = "#193A5D";
            greeting = "Good Evening, ";
        }
        window.setStatusBarColor(Color.parseColor(statusBarColor));
        imageView.setImageDrawable(getResources().getDrawable(drawableId));
        imageView.invalidate();

        TextView greetingTextview = (TextView) view.findViewById(R.id.greeting_textview);
        greetingTextview.setText(greeting + USERNAME);
        mLoadingDots.setTextColor(Color.parseColor(statusBarColor));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mDetectionSurface = null;
    }

    public void detectPattern() {
        mLoadingDots.setVisibility(View.VISIBLE);
        String out = ("detected: " + points.size() + "\n");
        for (Point p : points) {
            out += ("\t\t\t" + p.x + ", " + p.y + "\n");
        }
        out += "\n";

        int size = points.size();
        String name;
        if (size <= 2) {
            name = "lipitor";
        } else if (size <= 4) {
            name = "nexium";
        } else if (size <= 6){
            name = "abilify";
        } else {
            name = "lantis solostar";
        }

        tts.speak(name,TextToSpeech.QUEUE_FLUSH, null, null);
        isTTSSpeaking();
        mOutputTextView.setText(name);
    }

    public void isTTSSpeaking(){

        final Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                if (!tts.isSpeaking()) {
                    mLoadingDots.setVisibility(View.GONE);
                } else {
                    handler.postDelayed(this, 1000);
                }
            }
        };
        handler.postDelayed(r, 1000);
    }

    private class Point {
        float x;
        float y;

        public Point(float x, float y) {
            this.x = x;
            this.y = y;
        }
    }
}
